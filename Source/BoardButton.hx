package;

import flash.display.Sprite;
import flash.events.Event;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.text.TextFieldAutoSize;

class BoardButton extends Sprite
{
    var SIZE:Int = 40;
    
    public var open:Bool;
    
    var buttonSprite:Sprite;
    var valueSprite:Sprite;
    
    public var value:Int;
    public var column:Int;
    public var row:Int;
    
    public function new(value:Int, column:Int, row:Int)
    {
        super();
        
        this.value = value;
        this.column = column;
        this.row = row;
        
        open = false;
        build();
    }
    
    private function build():Void
    {
        buttonSprite = new Sprite();
        buttonSprite.graphics.beginFill(0x000000);
        buttonSprite.graphics.drawRect(0, 0, SIZE, SIZE);
        buttonSprite.graphics.endFill();
        buttonSprite.graphics.beginFill(0xCA8989);
        buttonSprite.graphics.drawRect(1, 1, SIZE-2, SIZE-2);
        
        valueSprite = new Sprite();
        valueSprite.graphics.beginFill(0x000000);
        valueSprite.graphics.drawRect(0, 0, SIZE, SIZE);
        valueSprite.graphics.endFill();
        valueSprite.graphics.beginFill(0x89CA89);
        valueSprite.graphics.drawRect(1, 1, SIZE-2, SIZE-2);
        
        if (value == -1)
        {
            valueSprite.graphics.beginFill(0xCC2222);
            valueSprite.graphics.drawCircle(SIZE/2, SIZE/2, 6);
            valueSprite.graphics.endFill();
        }
        else if (value > 0)
        {
            var format = new TextFormat("Arial", 15, 0, true);
            
            var text = new TextField();
            text.defaultTextFormat = format;
            text.setTextFormat(format);
            text.text = "" + value;
            text.autoSize = TextFieldAutoSize.LEFT;
            text.height = text.textHeight;
            text.x = (SIZE - text.width) / 2;
            text.y = (SIZE - text.height) / 2;
            valueSprite.addChild(text);
        }
        
        this.addChild(valueSprite);
        this.addChild(buttonSprite);
        
        buttonSprite.addEventListener("click", revealbuttonSprite);
    }
    
    private function revealbuttonSprite(e:Event):Void
    {
        reveal();
        dispatchEvent(new BoardEvent("reveal", value, column, row));
    }
    
    public function reveal():Void
    {
        open = true;
        buttonSprite.visible = false;
    }
    
}
