package;

import flash.events.Event;

class BoardEvent extends Event
{
    public var value:Int;
    public var column:Int;
    public var row:Int;
    
    public function new(e_type:String, value:Int, column:Int, row:Int)
    {
        super(e_type, false, false);
        
        this.value = value;
        this.column = column;
        this.row = row;
    }
}