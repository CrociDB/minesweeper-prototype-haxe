package;

import flash.display.Sprite;
import flash.Vector;
import flash.Lib;

class Board extends Sprite
{
    var logicBoard:LogicBoard;
    var buttons:Vector<Vector<BoardButton>>;
    
    var columns:Int;
    var rows:Int;
    var bombs:Int;
    
    public function new()
    {
        super();
        initBoard();
        
        x = (Lib.current.stage.stageWidth - width) / 2;
        y = (Lib.current.stage.stageHeight - height) / 2;
    }
    
    private function initBoard():Void
    {
        this.columns = 17;
        this.rows = 10;
        this.bombs = 30;
        
        logicBoard = new LogicBoard(columns, rows, bombs);
        logicBoard.printBoard();
        
        buttons = new Vector<Vector<BoardButton>>();
        for (i in 0...columns)
        {
            var buttonVector:Vector<BoardButton> = new Vector<BoardButton>();
            for (j in 0...rows)
            {
                var bb = new BoardButton(logicBoard.getValueAt(i, j), i, j);
                bb.addEventListener("reveal", revealButton);
                bb.x = i * bb.width;
                bb.y = j * bb.height;
                
                buttonVector.push(bb);
                addChild(bb);
            }
            buttons.push(buttonVector);
        }
    }
    
    private function revealButton(evt:BoardEvent):Void
    {
        startRevealing(evt.column, evt.row);
    }
    
    private function startRevealing(x:Int, y:Int):Void
    {
        reveal(x, y, 0, false);
    }
    
    private function reveal(x:Int, y:Int, count:Int, num:Bool):Void
    {
        var currNum:Bool = false;
        
        if (buttons[x][y].value == -1) return;
        
        if (count != 0)
        {
            if (buttons[x][y].open) return;
            
            buttons[x][y].reveal();
        }
        
        if (buttons[x][y].value > 0)
        {
            if (num == false)
            {
                currNum = true;
            }
            
            return;
        }
        
        count++;
        
        if (x > 0)
        {
            reveal(x-1, y, count, currNum);
            
            if (y > 0)
            {
                reveal(x-1, y-1, count, currNum);
            }
            
            if (y < rows-1)
            {
                reveal(x-1, y+1, count, currNum);
            }
        }
        
        if (y > 0)
        {
            reveal(x, y-1, count, currNum);
        }
        
        if (y < rows-1)
        {
            reveal(x, y+1, count, currNum);
        }
        
        if (x < columns-1)
        {
            reveal(x+1, y, count, currNum);
            
            if (y > 0)
            {
                reveal(x+1, y-1, count, currNum);
            }
            
            if (y < rows-1)
            {
                reveal(x+1, y+1, count, currNum);
            }
        }
    }
}





