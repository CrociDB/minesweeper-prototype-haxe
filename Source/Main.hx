package;

import flash.display.Sprite;
import flash.display.Stage;
import flash.display.*;
import flash.Lib;

class Main extends Sprite 
{
    var WIDTH:Int = 1152;
    var HEIGHT:Int = 720;
    
    var board:Board;
    
	public function new() 
    {	
		super();
        
        //Lib.current.stage.align = StageAlign.TOP_LEFT;
        //Lib.current.stage.scaleMode = StageScaleMode.NO_SCALE;
        
        costumize();
        
        board = new Board();
        addChild(board);
	}
    
    private function costumize()
    {
        graphics.beginFill(0x9999CA, 1.0);
        graphics.drawRect(0, 0, Lib.current.stage.stageWidth, Lib.current.stage.stageHeight);
        graphics.endFill();
        
        trace("HEY");
    }
}