package;

import flash.Vector;

class LogicBoard
{
    var width:Int;
    var height:Int;
    var bombs:Int;
    
    var board:Vector<Vector<Int>>;
    
    public function new(width:Int, height:Int, bombs:Int)
    {
        this.width = width;
        this.height = height;
        this.bombs = bombs;
        
        initBoard();
        plantBombs();
        evaluateBombs();
    }
    
    private function initBoard():Void
    {
        board = new Vector<Vector<Int>>();
        for (i in 0...width)
        {
            var row:Vector<Int> = new Vector<Int>();
            
            for (j in 0...height)
            {
                row.push(0);
            }
            
            board.push(row);
        }
    }
    
    private function plantBombs():Void
    {
        for (i in 0...bombs)
        {
            var ok:Bool = false;
            
            var randomX:Int = Std.random(width);
            var randomY:Int = Std.random(height);
            
            while (!ok)
            {
                if (board[randomX][randomY] == -1)
                {
                    randomX = Std.random(width);
                }
                else
                {
                    board[randomX][randomY] = -1;
                    ok = true;
                }
            }         
        }
    }
    
    private function evaluateBombs():Void
    {
        for (i in 0...width)
        {
            for (j in 0...height)
            {
                if (board[i][j] == -1) continue;
                
                if (i > 0)
                {
                    board[i][j] += board[i-1][j] == -1 ? 1 : 0;
                    
                    if (j < height-1)
                    {
                        board[i][j] += board[i-1][j+1] == -1 ? 1 : 0;
                    }
                    
                    if (j > 0)
                    {
                        board[i][j] += board[i-1][j-1] == -1 ? 1 : 0;
                    }
                }
                
                if (j < height-1)
                {
                    board[i][j] += board[i][j+1] == -1 ? 1 : 0;
                }
                
                if (j > 0)
                {
                    board[i][j] += board[i][j-1] == -1 ? 1 : 0;
                }
                
                if (i < width-1)
                {
                    board[i][j] += board[i+1][j] == -1 ? 1 : 0;
                    
                    if (j < height-1)
                    {
                        board[i][j] += board[i+1][j+1] == -1 ? 1 : 0;
                    }
                    
                    if (j > 0)
                    {
                        board[i][j] += board[i+1][j-1] == -1 ? 1 : 0;
                    }
                }
            }
        }
    }
    
    public function getValueAt(x:Int, y:Int):Int
    {
        return board[x][y];
    }
    
    public function printBoard():Void
    {
        var line:String = "BOARD\n";
        
        for (i in board)
        {
            for (j in i)
            {
                line += j + ", ";
            }
            line += "\n";
        }
        
        trace(line);
    }
}